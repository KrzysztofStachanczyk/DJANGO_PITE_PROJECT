$('#jq_surveyAnswer_check').on('submit', function(event){
    event.preventDefault();

    $.ajax({
        url : "hidden/jqSurveyAnswer/checkIfUserIdIsCorrect",
        type : "POST",
        data : $("#jq_surveyAnswer_check").serialize(),

        success : function(json) {

            if(json['success']==true){
                var target = "hidden/jqSurveyAnswerForm?userID="+$("#surveyAnswerUserId").val();
                console.log(target);
                $.get(target,function(data) {
                    $("#pageHeader").html($(data).filter('#pageHeader'));
                    $("#pageContent").html($(data).filter('#pageContent'));
                });
            }

            else{
                markFieldAsIncorrect("surveyAnswerUserId",json['errorMSG'])
            }
        }
    });
});


$('#jq_surveyAnswer_form').on('submit', function(event){
    event.preventDefault();
    $.ajax({
        url : "hidden/jqSurveyAnswerForm/saveAnswer",
        type : "POST",
        data : $("#jq_surveyAnswer_form").serialize(),

        success : function(json) {
            markFieldAsUnknownState("surveyAnswerName");
            markFieldAsUnknownState("surveyAnswerSurname");
            markFieldAsUnknownState("surveyAnswerEmail");

            if(json['success']==true){
                var toLoad = "hidden/jqSurveyAnswerForm?userID="+ json['userID']+"&answerID="+json['answerID'];

                $.get(toLoad,function(data) {
                    $("#pageHeader").html($(data).filter('#pageHeader').children());
                    $("#pageContent").html($(data).filter('#pageContent').children());
                });
            }
            else{
                if(json['fields']!=null){
                    var fieldMessages=Object.keys(json["fields"]);

                    fieldMessages.forEach(function(key){
                        markFieldAsIncorrect(key,json["fields"][key]);
                        });
                }
                else{
                    toastr.error(json['errorMSG']);
                }
            }

        }
    });
});