

    /* zmienne globalne 
    var old_comments=[]; - komentarze znajdujace sie aktualnie na stronie
    var chat_request_timeout; - do usunieci timeouta
    var js_user_id = "{{userID}}"; - userID do identyfikacji komentarzy
    var js_admin_id ="{{adminID}}"; -adminID do usuwania komentarzy
*/
 
 $('#jq_survey_add_comment_form').on('submit', function(event){
            event.preventDefault();
            var form = document.getElementById("jq_survey_add_comment_form");
            $.ajax({
                    url : "hidden/jqSurveyChatAddComment",
                    type : "POST",
                    data : {
                        nickname: $('#nickname').val(),
                        userID: $('#userID').val(),
                        comment_content: $('#comment_content').val(),
                        email: $('#email').val(),
                        captcha_0:$(form).find('input[name="captcha_0"]').val(),
                        captcha_1: $(form).find('input[name="captcha_1"]').val(),
                        csrfmiddlewaretoken: getCookie('csrftoken')
                    },

                    success : function(json) {
                        if(json["errortoken"]){
                            toastr.error(json["errortoken"]);
                        }
                        clearTimeout(chat_request_timeout);
                        ajax_chat_request(false);
                        reloadcaptha();
                    }
            });
            
      });
      
      
function delete_comment(comment){
        var clas = comment.className;
        var len=clas.length;
        var com_id="";
        while(true){
            if(clas[len-1] !== '-'){
                com_id+=clas[len-1];
                len--;
            }
            else{
                break;
            }
        }
        function reverse(s){
            return s.split("").reverse().join("");
        }
        com_id=reverse(com_id);

    $.ajax({
                    url : "hidden/jqSurveyChatDeleteComment",
                    type : "POST",
                    data : {
                        userID: js_user_id,
                        adminID: js_admin_id,
                        commentID: com_id,
                        csrfmiddlewaretoken: getCookie('csrftoken')
                    },

                    success : function(json) {
                        clearTimeout(chat_request_timeout);
                        ajax_chat_request(false);
                    }
            });
}     
      
    
function ajax_chat_request(showInfo){
            $.ajax({
                    url : "hidden/jqSurveyChat",
                    type : "POST",
                    data : {
                        userID: js_user_id,
                        csrfmiddlewaretoken: getCookie('csrftoken')
                    },

                    success : function(json) {
                        var comments = json["comments"];
                        var number=comments.length;
                        /* check changes */
                        if(!compare_arrays(old_comments,comments)){
                            if(showInfo){
                                toastr.info("Otrzymano nową wiadomość");
                            }
                            old_comments=comments;
                            $('#chat-header').empty();
                            $("#chat-header").prepend("Liczba Komentarzy: "+number);
                            $('#chat-survey').empty();
                            for(var i=0,len=comments.length;i<len;i++){
                            $('#chat-survey').prepend(generate_comment_code(comments[i]));
                            }
                            $('textarea').autoHeight();
                        }
                    }
            }).complete(function(){
        chat_request_timeout=setTimeout(function(){ajax_chat_request(true);}, 5000);
    }).responseText;
            }
    $( document ).ready(function() {
            ajax_chat_request(false);
      });

      
      
      

function format_date(django_date){
        var new_date="";
        for (var i=0,len=django_date.length;i<len;i++){
            if(django_date[i] === '.'){break;}
            new_date=new_date+django_date[i];
        }
        return new_date;
    }
    
function compare_arrays(array1,array2){
        if(array1.length !== array2.length){return false;}
        for(var i=0,len=array1.length;i<len;i++){
                if(array1[i][2] !== array2[i][2]){return false;}
            
        }
    return true;
}

function generate_comment_code(comment){
        var code='';
        code+='<div id="comment" class="comment container-fluid">';
        code+='<img type="image" src="/static/images/cross.jpg" class="del-comment comment-nr-'+comment[3]+'" id="del-comment" ';
        code+=' onclick="delete_comment(this)"/>';
        code+='<div class="comment_nick" id="comment_nick"><strong>';
        code+=comment[1]+'</strong></div>';
        code+='<textarea readonly id="text_comment" class="text-comment">';
        code+=comment[0]+'</textarea>';
        code+='<div class="comment_date" id="comment-date"><i>';
        code+=format_date(comment[2])+'</i></div></div>';
        return code;
    }
    
    