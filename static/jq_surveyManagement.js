$('#surveyEditForm').on('submit', function(event){
    event.preventDefault();
    $.ajax({
        url : "hidden/jqSurveyManagementForm/saveSurvey",
        type : "POST",
        data : $("#surveyEditForm").serialize(),

        success : function(json) {
            console.log(json);
            if(json['success']==true){
                var toLoad = "hidden/jqSurveyManagementForm?adminID="+ json['adminID'];
                $.get(toLoad,function(data) {
                    $("#pageHeader").html($(data).filter('#pageHeader').children());
                    $("#pageContent").html($(data).filter('#pageContent').children());
                });
            }
            else{
                toastr.error(json['errorMSG']);
            }

        }
    });
});