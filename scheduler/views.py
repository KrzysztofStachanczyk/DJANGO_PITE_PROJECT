from django.shortcuts import render

# Create your views here.
def main(request):
    return render(request, 'main.html')

def jqMain(request):
    return render(request,'jq_base.html')

def jqNavBar(request):
    return render(request,'jq_navBar.html')

def jqDefault(request):
    return render(request,'jq_main.html')



def jqRegisterPanel(request):
    return render(request,'jq_register_panel.html',{'fields': [
        ('name','inputName','Imię:','text'),
        ('surname','inputSurname','Nazwisko:','text'),
        ('email','inputEmail','Email','text'),
        ('password','inputPassword','Hasło:','password'),
        ('rePassword','inputPasswordRetype', 'Powtórz hasło:', 'password')
    ]})


def jqTermsAndConditions(request):
    return render(request,'jq_terms_and_conditions.html')