# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0011_userview'),
    ]

    operations = [
        migrations.CreateModel(
            name='SurveyComment',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('nick', models.CharField(max_length=101, default='unknown')),
                ('text', models.TextField(max_length=200)),
                ('creation_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
                ('userID', models.CharField(max_length=10)),
                ('commentID', models.CharField(max_length=10)),
                ('survey', models.ForeignKey(to='scheduler.Survey')),
            ],
        ),
    ]
