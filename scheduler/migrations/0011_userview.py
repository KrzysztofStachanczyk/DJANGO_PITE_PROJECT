# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0010_answer_creation_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserView',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('view_date', models.DateTimeField(default=datetime.datetime.now, blank=True)),
            ],
        ),
    ]
