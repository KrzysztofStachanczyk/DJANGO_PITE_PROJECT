from django.conf.urls import url
from .views import *

from .views_set.pdf_report import pdfReport
from .views_set.csv_report import csvReport
from .views_set.html_report import htmlReport

from .views_set.jq_register_panel import registerAjaxRequest
from .views_set.jq_login_elements import jqLoginPane, logoutRequestAjax, loginRequestAjax
from .views_set.jq_global_statistic import jqGlobalStatistic
from .views_set.jq_contact import jqContactForm, jqContactSentMailRequest
from .views_set.jq_restore_password import jqRestorePassword, jqSentRestoreEmailRequest, jqSentNewPasswordRequest
from .views_set.jq_account_management import jqAccountManagement, jqAccountChangeUserdataRequest, jqAccountChangeUserPasswordRequest, jqAccountRemoveSurveyRequest
from .views_set.jq_surveyAnswer import jq_surveyAnswer, jqSurveyAnswerCheckUserIdRequest, jq_SurveyAnswerForm, jq_SurveyAnswerRedirector , jq_SurveyAnswerForm_save
from .views_set.jq_surveyManager import jq_SurveyManagerRedirector, jq_SurveyManagerForm, jq_SurveyManagerForm_save
from .views_set.jq_global_statistic import addViewNumberRequestAjax
from .views_set.jq_get_plot_data import plotDataRequestAjax
from .views_set.jq_survey_chat import jqSurveyChatMain, jqSurveyChatAddComment, jqSurveyChatDeleteComment

urlpatterns = [
    url(r"^csv_report",csvReport,name="csvReport"),
    url(r'^pdf_report', pdfReport, name='pdfReport'),
    url(r'^html_report', htmlReport, name='htmlReport'),

    # jqery page version
    url(r'^$',jqMain, name="jq_main"),
    url(r'^hidden/jqNavBar$',jqNavBar, name="jq_navBar"),
    url(r'^hidden/jqDefault$',jqDefault,name="jq_default"),
    url(r'^hidden/jqLoginPane$',jqLoginPane,name="jq_loginPane"),
    url(r'^hidden/jqRegisterPanel$',jqRegisterPanel,name="jq_registerPanel"),
    url(r'^hidden/jqRegisterPanel/registerRequest$',registerAjaxRequest,name="jq_registerPanel_registerRequest"),
    url(r'^hidden/jqLoginElements/logoutRequest$',logoutRequestAjax,name="jq_loginElements_logoutRequest"),
    url(r'^hidden/jqTermsAndConditions$',jqTermsAndConditions,name="jq_termsAndConditions"),
    url(r'^hidden/jqGlobalStatistic$',jqGlobalStatistic, name="jq_globalStatistic"),
    url(r'^hidden/jqLoginElements/loginRequest$', loginRequestAjax, name="jq_loginElements_loginRequest"),
    url(r'^hidden/jqContactForm$',jqContactForm,name="jq_contactForm"),
    url(r'^hidden/jqContactForm/contactSentMailRequest$', jqContactSentMailRequest, name="jq_contact_sentMailRequest"),
    url(r'^hidden/jqRestorePassword$',jqRestorePassword,name="jq_restorePassword"),
    url(r'^hidden/jqRestorePassword/sentRestoreEmailRequest$',jqSentRestoreEmailRequest, name="jq_restorePassword_sentRestoreEmailRequest"),
    url(r'^hidden/jqRestorePassword/sentNewPasswordRequest$',jqSentNewPasswordRequest,name="jq_restorePassword_sentNewPasswordRequest"),
    url(r'^hidden/jqAccountManagement$',jqAccountManagement,name="jq_accountManagement"),
    url(r'^hidden/jqAccountManagement/sentChangeUserdataRequest$',jqAccountChangeUserdataRequest, name="jq_accountManagement_changeUserData"),
    url(r'^hidden/jqAccountManagement/sentChangeUserPasswordRequest$', jqAccountChangeUserPasswordRequest,name="jq_accountManagement_changeUserPassword"),
    url(r'^hidden/jqAccountManagement/sentRemoveSurveyRequest$', jqAccountRemoveSurveyRequest,name="jq_accountManagement_removeSurvey"),
    url(r'^hidden/jqSurveyAnswer$',jq_surveyAnswer,name="jq_surveyAnswer"),
    url(r'^hidden/jqSurveyAnswer/checkIfUserIdIsCorrect$',jqSurveyAnswerCheckUserIdRequest,name="jq_surveyAnswer_checkUserId"),
    url(r'^jqSurveyAnswerForm',jq_SurveyAnswerRedirector,name="surveyAnswerFormRedirector"),
    url(r'^hidden/jqSurveyAnswerForm/saveAnswer$',jq_SurveyAnswerForm_save,name="jq_surveyAnswerForm_save"),
    url(r'^hidden/jqSurveyAnswerForm',jq_SurveyAnswerForm, name="surveyAnswerForm"),
    url(r'^jqSurveyManagementForm', jq_SurveyManagerRedirector, name="surveyManagementFormRedirector"),
    url(r'^hidden/jqSurveyManagementForm/saveSurvey$',jq_SurveyManagerForm_save,name="jq_surveyManagementForm_save"),
    url(r'^hidden/jqSurveyManagementForm',jq_SurveyManagerForm, name="jq_surveyManagementForm"),

    #visitorscounter
    url(r'^hidden/jqAddVisitor$',addViewNumberRequestAjax,name="addViewNumberRequestAjax"),
    #getplotdata
    url(r'^hidden/jqGetPlotData$',plotDataRequestAjax,name="plotDataRequestAjax"),

    #surveychat
    url(r'^hidden/jqSurveyChat$', jqSurveyChatMain, name="jq_SurveyChatMain"),
    url(r'^hidden/jqSurveyChatAddComment$', jqSurveyChatAddComment, name="jq_SurveyChatAddComment"),
    url(r'^hidden/jqSurveyChatDeleteComment$',jqSurveyChatDeleteComment,name="jqSurveyChatDeleteComment"),
]