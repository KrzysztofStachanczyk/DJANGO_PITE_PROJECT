from django.apps import AppConfig
from menu import Menu, MenuItem
from django.core.urlresolvers import reverse
class MyAppConfig(AppConfig):
	name = 'scheduler'
	verbose_name = "on_start"
