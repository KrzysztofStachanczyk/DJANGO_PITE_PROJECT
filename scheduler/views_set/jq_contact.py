from django.shortcuts import render
from ..models import *
# from django.core.mail import send_mail
from django import forms
from captcha.fields import CaptchaField

from django.http import HttpResponse

import json
import re


class CaptchaTestForm(forms.Form):
    captcha = CaptchaField()

def debug_sent_mail(title,message,connectedEmail,target,fail_silently):
    '''
    Funkcja do nadpisywania send_mail przy debugowaniu
    :param title:
    :param message:
    :param connectedEmail:
    :param target:
    :param fail_silently:
    :return:
    '''
    print("Sending contact mail to: " + str(target) + "\n\ttitle:" + str(title) + "\n\tmessage:" + str(message))


def jqContactForm(request):
    '''
    Renderuje formularz do kontaktu z administracją
    :param request:
    :return:
    '''

    form = CaptchaTestForm()
    if 'member_id' in request.session:
        row = SurveyUser.objects.get(id=request.session['member_id'])
        return render(request, "jq_contact.html", {
            'fields': [
                ('contactName', 'Imię:', 'text', row.name, False),
                ('contactSurname', 'Nazwisko:', 'text', row.surname, False),
                ('contactEmail', 'Email:', 'text', row.email, False),
                ('contactTopic', 'Temat:', 'text', "", True)
            ],
            'form': form
        })
    else:

        return render(request, "jq_contact.html", {
            'fields': [
                ('contactName', 'Imię:', 'text', '', True),
                ('contactSurname', 'Nazwisko:', 'text', '', True),
                ('contactEmail', 'Email:', 'text', '', True),
                ('contactTopic', 'Temat:', 'text', "", True)
            ],
            'form': form
        })


def jqContactSentMailRequest(request):
    '''
    Wysyła maila do administracji
    :param request: żądanie POST z polami:
        - email
        - name
        - surname
        - topic
        - text
    :return:
    JSON z informacją o wyniku operacji:
        - successful
        - fieldState - komunikaty dla pól:
            - contactName
            - contactSurname
            - contactEmail
            - contactTopic
        - errorMSG - ogólny komunikat o błędzie
    '''

    # TODO usun jesli skonfigurowano klienta (w settings.py)
    send_mail = debug_sent_mail

    if request.method == 'POST':
        # zapisz dane do zmiennych lokalnych
        form = CaptchaTestForm(request.POST)

        email = request.POST['email']
        name = request.POST['name']
        surname = request.POST['surname']
        topic = request.POST['topic']
        text_message = request.POST['text']

        fieldState = dict()
        allCorrect = True

        # walidacja poprawności wpisanych danych
        if not re.match(
                r"^[A-Za-z àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆŠŽ]{3,20}$",
                name):
            allCorrect = False
            fieldState["contactName"] = "Imię musi składać się z 3-20 liter"

        if not re.match(
                r"^[A-Za-z àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆŠŽ∂ð,.'-]{3,50}$",
                surname):
            allCorrect = False
            fieldState['contactSurname'] = "Nazwisko musi składać się z 3-50 liter i znaków"

        if not re.match(
                r"^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$",
                email):
            allCorrect = False
            fieldState['contactEmail'] = "Wpisz e-mail w poprawnym formacie"

        if not topic.isprintable():
            allCorrect = False
            fieldState['contactTopic'] = "Korzysztaj tylko z znaków drukowalnych "

        if len(topic) < 3 or len(topic) > 50:
            allCorrect = False
            fieldState['contactTopic'] = "Długość tematu powinna być w zakresie [3,50]"

        # sprawdzanie captcha
        if not form.is_valid():
            return HttpResponse(
                json.dumps({
                    "errorMSG": "Błąd captcha",
                    "successful": False
                }),
                content_type="application/json"
            )


        if allCorrect == False:
            # informowanie o błędnych polach
            return HttpResponse(
                json.dumps({
                    "fieldState": fieldState,
                    "errorMSG": "Błąd wprowadzonych danych",
                    "successful": False
                }),
                content_type="application/json"
            )

        # generowanie i wysyłanie wiadomości
        text_message = text_message + '\n\n' + 'This message was send by ' + name + ' ' + surname

        try:
            send_mail(
                topic,
                text_message,
                # TODO zamien na skonfigurowany adres email
                'powiazany email',
                # TODO zamien na adres admina
                ['admin email'],
                fail_silently=False,
            )
        except:
            # informowanie o błędach wysyłania email
            return HttpResponse(
                json.dumps({
                    "errorMSG": "Błąd wysyłania danych",
                    "successful": False
                }),
                content_type="application/json"
            )

        # zwracanie komunikatu o poprawnym zakończeniu operacji
        return HttpResponse(
            json.dumps({"fieldState": fieldState,
                        "successful": True
                        }),
            content_type="application/json"
        )

    else:
        return HttpResponse(
            json.dumps({
                "errorMSG": "Błąd metody przekazywania danych",
                "successful": False
            }),
            content_type="application/json"
        )
