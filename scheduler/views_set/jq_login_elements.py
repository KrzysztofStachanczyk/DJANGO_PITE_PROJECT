from django.contrib.auth.hashers import make_password
from django.shortcuts import render
from ..models import SurveyUser
from django.http import HttpResponse

import json


def jqLoginPane(request):
    '''
    Renderuje panel logowania
    '''
    return render(request,'jq_loginPane.html',{
        'fields': [
            ("email","inputLoginEmail","Email:","text"),
            ("password","inputLoginPassword","Hasło:","password")
        ]
    })


def logoutRequestAjax(request):
    '''
    Wylogowuje użytkownika
    '''

    # usun wszystkie zmienne sesyjne powiazane z uzytkownikiem
    try:
        del request.session['member_id']
        del request.session['member_name']
        del request.session['member_surname']
    except KeyError:
        pass

    return HttpResponse(
        json.dumps({}),
        content_type="application/json"
    )


def loginRequestAjax(request):
    '''
    Procedura realizujaca logowanie

    request powinien zawierać wysłane przy pomocy mechanizmu POST dane:
        - email
        - password

    zwraca JSON-a z informacją o wyniku operacji zawierającego pole successfullLogin

    jeżeli wystąpił błąd logowania także pola
        - errorMSG - ogólny opis problemu
        - fieldState - słownik z komunikatami o błędach dla konkretnych pól
            - inputLoginEmail
            - inputLoginPassword
    '''

    if request.method == 'POST':
        fieldState = dict()

        # pobierz rekord usera o zadanym adresie e-mail
        try:
            user = SurveyUser.objects.get(email=request.POST['email'])
        except:
            fieldState["inputLoginEmail"] = "Nieprawidłowy adres e-mail"

        # jeżeli istnieje to wygeneruj skrót hasła (MD5)
        else:
            hashed = make_password(password=request.POST['password'],
                                   salt=None,
                                   hasher='unsalted_md5')

            # porównaj wygenerowaną z zapisaną w bazie
            if user.hash_of_password == hashed:

                # ustaw wymagane zmienne sesyjne
                request.session['member_id'] = user.id
                request.session['member_name'] = user.name
                request.session['member_surname'] = user.surname

                # zwróć komunikat o udanym logowaniu
                return HttpResponse(
                    json.dumps({
                        "successfullLogin": True
                    }),
                    content_type="application/json"
                )
            else:
                fieldState["inputLoginPassword"] = "Błędne hasło"

        # zwróć komunikat z informacjami o błędach
        return HttpResponse(
            json.dumps({"fieldState": fieldState,
                "successfullLogin": False,
                "errorMSG": "Błąd logowania"
            }),
            content_type="application/json"
        )

    else:
        return HttpResponse(
            json.dumps({"errorMSG": "Błąd metody przekazywania danych"}),
            content_type="application/json"
        )
