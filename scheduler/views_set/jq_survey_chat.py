from django.http import HttpResponse
import json
from django.core.serializers.json import DjangoJSONEncoder
from ..models import SurveyComment,Survey,SurveyUser
from django.core.exceptions import ObjectDoesNotExist
import random
import string
from django.shortcuts import render
from django import forms
from captcha.fields import CaptchaField

class CaptchaTestForm(forms.Form):
    captcha = CaptchaField()



def idGenerator(size=8, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def jqSurveyChatMain(request):
    if request.method == 'POST':
       
        try:
            comments= SurveyComment.objects.all().filter(userID=request.POST['userID']).order_by('-creation_date')
            comments_text=[]
            for i in comments:
                comments_text.append((i.text,i.nick,str(i.creation_date),i.commentID))
        except ObjectDoesNotExist:
            return HttpResponse(
            json.dumps({"comments": []}),
            content_type="application/json"
            )
        except:
            return HttpResponse(
            json.dumps({"errorMSG": "Błąd metody przekazywania danych"}),
            content_type="application/json"
            )
        return HttpResponse(
            json.dumps({"comments": list(comments_text)}),
            content_type="application/json"
            )

def jqSurveyChatAddComment(request):
    if request.method == 'POST':
        try:
            form = CaptchaTestForm(request.POST)
            if form.is_valid():
                survey_db=Survey.objects.get(userID=request.POST['userID'])
                nickname=request.POST['nickname']
                while True:
                    commentID = idGenerator()
                    try:
                        # make sure that adminID is unique
                        q1=SurveyComment.objects.filter(userID=request.POST['userID'])
                        q1.get(commentID=commentID)
                    except:
                        break
                row=SurveyComment(
                survey=survey_db,
                nick=nickname,
                text=request.POST['comment_content'],
                userID=request.POST['userID'],
                commentID=commentID
                )
                row.save()
            else:
                return HttpResponse(
            json.dumps({"errortoken" : "Błędny kod Captcha."}),
            content_type="application/json"
            )
        except ObjectDoesNotExist:
            return HttpResponse(
            json.dumps({"comments": []}),
            content_type="application/json"
            )
        except ValueError:
            return HttpResponse(
            json.dumps({"errorMSG": "Błąd metody przekazywania danych"}),
            content_type="application/json"
            )
        
        return HttpResponse(
            json.dumps({}),
            content_type="application/json"
            )
            


    
def jqSurveyChatDeleteComment(request):
    if request.method == 'POST':
        try:
            adminID=request.POST['adminID']
            commentID=request.POST['commentID']
            userID=request.POST['userID'];
            row=Survey.objects.get(adminID=adminID)
            if(row.user.id!=request.session['member_id']):
                return HttpResponse(
                    json.dumps({"errorMSG": "Nie masz uprawnień do usuwania komentarzy"}),
                    content_type="application/json"
                    )
            q1=SurveyComment.objects.filter(userID=userID)
            for i in q1:
                print(i.commentID)
            print('  ',commentID)
            q1.get(commentID=commentID).delete()
            return HttpResponse(
            json.dumps({}),
            content_type="application/json"
            )
        except:
            return HttpResponse(
            json.dumps({"errorMSG": "Błąd metody przekazywania danych"}),
            content_type="application/json"
            )