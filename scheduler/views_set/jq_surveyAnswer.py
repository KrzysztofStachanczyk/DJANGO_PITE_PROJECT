from django.shortcuts import render
from ..models import *
import string
import random
from dateutil import parser
from django.core.urlresolvers import reverse
import json
from django.http import HttpResponse
import re
from captcha.fields import CaptchaField
from django import forms

class CaptchaTestForm(forms.Form):
    captcha = CaptchaField()

def idGenerator(size=8, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def jq_surveyAnswer(request):
    return render(request, "jq_surveyAnswer.html")

def jqSurveyAnswerCheckUserIdRequest(request):
    '''
    Sprawdza czy istnieje ankieta o podanym userID
    :param request: żądanie post zawierajace pole:
        - userID
    :return:
    JSON z informacją zwrotną:
        - success - True jeśli ankieta istnieje
        - errorMSG - komunikat o błędzie
    '''
    if request.method=="POST":
        try:
            #znajdź i pobierz wiersz o podanym userID
            row = Survey.objects.get(userID=request.POST['userID'])

            return HttpResponse(
                json.dumps({
                    "success": True}),
                content_type="application/json"
            )
        except:
            return HttpResponse(
                json.dumps({
                    "success" : False,
                    "errorMSG": "Ankieta o podanym id nie istnieje"}),
                content_type="application/json"
            )
    else:
        return HttpResponse(
            json.dumps({
                "success": False,
                "errorMSG": "Błąd metody przekazywania danych"
            }),
            content_type="application/json"
        )

def jq_SurveyAnswerForm(request):
    '''
    Renderuje formularz o zadanym userID
    :param request: żądanie z przekazanym metodą GET userID i opcjonalnie answerID (identyfikator odpowiedzi)
    :return:
    '''
    if 'userID' in request.GET.keys():
        try:
            # pobierz wiersz zawierający opis ankiety
            row = Survey.objects.get(userID=request.GET['userID'])
        except:
            return render(request, "jq_surveyAnswer.html")

        dateTimes = []

        # wygeneruj listę opcji
        for fromTime, toTime in row.fields:
            dateTimes.append((fromTime, toTime, False))

        name = ""
        surname = ""
        email = ""
        answerID = None
        editURL = None

        # jeżeli otworzono ankietę to modyfikacji uzupełnij wypełnione pola
        if 'answerID' in request.GET.keys():
            answerID = request.GET['answerID']
            try:
                answerRow = Answer.objects.get(answerID=request.GET['answerID'])
                name = answerRow.name
                surname = answerRow.surname
                email = answerRow.email

                for i in range(0, len(dateTimes)):
                    fromTime, toTime, *other = dateTimes[i]

                    if (fromTime, toTime) in answerRow.answer.keys():
                        dateTimes[i] = (fromTime, toTime, answerRow.answer[(fromTime, toTime)])

                #wygeneruj link do edycji odpowiedzi
                editURL = request.build_absolute_uri(reverse('surveyAnswerFormRedirector')) + "?userID=" + request.GET[
                    'userID'] + "&answerID=" + answerID
            except:
                pass

        form = CaptchaTestForm()
        return render(request, 'jq_surveyAnswerForm.html', {'title': row.title,
                                                            'creationDate': row.creation_date,
                                                            'description': row.description,
                                                            'dateTimes': dateTimes,
                                                            'userID': request.GET['userID'],
                                                            'answerID': answerID,
                                                            'name': name,
                                                            'surname': surname,
                                                            'email': email,
                                                            'editURL': editURL,
                                                            'capthaform': form
                                                            })

    else:
        return render(request, "jq_surveyAnswer.html")

def jq_SurveyAnswerForm_save(request):
    '''
    Zapisuje odpowiedź do bazy danych
    :param request: żądanie typu POST z parametrami:
        - name
        - surname
        - surveyAnswerEmail
        - userID
        -
    :return:
    JSON - zawierający wynik operacji z polami:
        - success
        - errorMSG - ogólny opis błędu
        - fields - słownik zawierający komunikaty o błędach dla pół:
            - surveyAnswerName
            - surveyAnswerSurname
            - surveyAnswerEmail
        - answerID, userID - jeżeli wprowadzanie zakończone pomyślnie
    '''
    if request.method == 'POST':
        if 'userID' not in request.POST.keys():
            return HttpResponse(
                json.dumps(
                    {
                        "success": False,
                        "errorMSG": "Nie przekazano identyfikatora ankiety"
                    }
                ),
                content_type="application/json"

            )

        # walidacja poprawności pól
        fieldState = dict()
        allCorrect = True

        if not re.match(
                r"^[A-Za-z àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆŠŽ]{3,20}$",
                request.POST['name']):
            allCorrect = False
            fieldState["surveyAnswerName"] = "Imię musi składać się z 3-20 liter"

        if not re.match(
                r"^[A-Za-z àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆŠŽ∂ð,.'-]{3,50}$",
                request.POST['surname']):
            allCorrect = False
            fieldState['surveyAnswerSurname'] = "Nazwisko musi składać się z 3-50 liter i znaków"

        if not re.match(r"^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$",request.POST['email']):
            allCorrect=False
            fieldState['surveyAnswerEmail'] = "Nieprawidłowy format email"

        if allCorrect:
            try:
                answerID = None
                answerRow = None

                surveyRow = Survey.objects.get(userID=request.POST['userID'])

                if 'answerID' in request.POST.keys():
                    answerRow = Answer.objects.get(answerID=request.POST['answerID'])
                    answerID = request.POST['answerID']

                else:
                    answerRow = Answer()

                    while True:
                        answerID = idGenerator()
                        try:
                            Answer.objects.get(answerID=answerID)
                        except:
                            break
                    answerRow.answerID=answerID

                answerRow.survey = surveyRow
                answerRow.name = request.POST['name']
                answerRow.surname = request.POST['surname']
                answerRow.email = request.POST['email']

                listOfAnswers = dict()

                # odczyt odpowiedzi z pól
                for field in request.POST.keys():
                    if '|' in field:
                        try:
                            datesInString = field.split('|')
                            fromDate = parser.parse(datesInString[0])
                            toDate = parser.parse(datesInString[1])
                            answer = request.POST[field]
                            listOfAnswers[(fromDate), (toDate)] = answer
                        except:
                            pass

                listOfValidatedAnswers = dict()
                for element in surveyRow.fields:
                    if element in listOfAnswers.keys():
                        listOfValidatedAnswers[element] = listOfAnswers[element]
                    else:
                        listOfValidatedAnswers[element] = False

                answerRow.answer = listOfValidatedAnswers
                answerRow.save()

                return HttpResponse(
                    json.dumps(
                        {
                            "success": True,
                            "answerID" : answerID,
                            "userID" : request.POST['userID']
                        }
                    ),
                    content_type="application/json"
                )

            except:
                return HttpResponse(
                    json.dumps(
                        {
                            "success": False,
                            "errorMSG": "Błąd wprowadzania danych do bazy"
                        }
                    ),
                    content_type="application/json"
                )

        else:
            return HttpResponse(
                json.dumps({
                    "success": False,
                    "errorMSG": "Błąd sprawdzania poprawności danych",
                    "fields": fieldState
                }),
                content_type="application/json"
            )
    else:
        return HttpResponse(
            json.dumps({
                "success": False,
                "errorMSG": "Błąd metody przekazywania danych"}),
            content_type="application/json"
        )


def jq_SurveyAnswerRedirector(request):
    '''
    Ładuje odpowiednią stronę na podstawie linku dostępu do odpowiadania ankiety
    :param request:
    :return:
    '''
    if 'userID' in request.GET.keys():
        url=""
        if 'answerID' in request.GET.keys():
            url = "hidden/jqSurveyAnswerForm?userID=" + request.GET['userID'] + "&answerID=" + request.GET['answerID']
        else:
            url = "hidden/jqSurveyAnswerForm?userID=" + request.GET['userID']

        return render(request,"jq_base.html",{"loadPage":url})

    else:
        url = "hidden/jqSurveyAnswer"
        return render(request,"jq_base.html",{"loadPage":url})