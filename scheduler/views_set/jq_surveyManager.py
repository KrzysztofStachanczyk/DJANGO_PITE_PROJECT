from django.shortcuts import render
from ..models import *
import string
import random
from dateutil import parser
from django.core.urlresolvers import reverse
import json
from django.http import HttpResponse
from captcha.fields import CaptchaField
from django import forms

class CaptchaTestForm(forms.Form):
    captcha = CaptchaField()

def idGenerator(size=8, chars=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def jq_SurveyManagerForm(request):
    '''
    Renderuje widok tworzenia i modyfikacji ankiety
    :param request: przekazany metodą GET parametr adminID
    :return:
    '''
    if 'adminID' in request.GET.keys():
        try:
            row = Survey.objects.get(adminID=request.GET['adminID'])
        except:
            return render(request, "jq_surveyManagement.html",
                          {'errorDescription': "Podany przez Ciebie numer ID nie występuje w bazie ankiet"})

        if row.user.id != request.session['member_id']:
            return render(request, "jq_surveyManagement.html",
                          {'errorDescription': "Nie masz uprawnienień do edycji tej ankiety"})

        dateTimes = []
        id1 = 0

        try:
            if row.fields is not None:
                for fromDate, toDate in row.fields:
                    dateTimes.append((id1, fromDate, toDate))
                    id1 =id1+1

        except:
            return render(request, "jq_surveyManagement.html",
                          {'errorDescription': "Nie udało się wczytać informacji o wcześniejszych wyborach"})

        # Append extra 5 fields
        for i in range(0,5):
            dateTimes.append((i+id1,None,None))

        adminURL = request.build_absolute_uri(reverse('surveyManagementFormRedirector')) + "?adminID=" + row.adminID
        userURL = request.build_absolute_uri(reverse('surveyAnswerFormRedirector')) + "?userID=" + row.userID

        listOfAnswers = []
        answers = Answer.objects.filter(survey=row)

        users = {}
        anyMatches = False
        for fromDate, toDate in row.fields:
            listOfMatches = []
            for answer in answers:
                try:
                    if answer.answer[(fromDate, toDate)]:
                        anyMatches = True
                        listOfMatches.append((answer.name, answer.surname, answer.email))
                        users[(answer.name, answer.surname, answer.email)] = True
                except:
                    pass
            listOfAnswers.append((fromDate, toDate, listOfMatches, len(listOfMatches)))

        form = CaptchaTestForm()
        return render(request, "jq_surveyManagement.html", {'title': row.title,
                                                             'description': row.description,
                                                             'creationDate': row.creation_date,
                                                             'userID': row.userID,
                                                             'adminID': row.adminID,
                                                             'answerDates': listOfAnswers,
                                                             'answerUsers': users,
                                                             'adminURL': adminURL,
                                                             'userURL': userURL,
                                                             'capthaform': form,
                                                             'dateTimes': dateTimes})

    else:
        dateTimes = [(i,None,None) for i in range(0,10)]

        return render(request, "jq_surveyManagement.html", {'dateTimes': dateTimes})

def jq_SurveyManagerRedirector(request):
    '''
    Ładuje odpowiednie kontenery na podstawie linku
    :param request:
    :return:
    '''
    if 'adminID' in request.GET.keys():
        url= "hidden/jqSurveyManagementForm?adminID=" + request.GET['adminID']
        return render(request,"jq_base.html",{"loadPage":url})
    else:
        return render(request,"jq_base.html",{})


def jq_SurveyManagerForm_save(request):
    '''
    Zapisuje zmiany w ankiecie / tworzy nową
    :param request: żądanie typu POST
        - adminID - jeżeli modyfikowana jest istniejąca ankieta
        - title
        - description
        - pola z opcjami wyboru (dateTimeFrom_X ,dateTimeTo_Y)
    :return:
    JSON - zawierający informacje o powodzeniu operacji:
        - success
        - errorMSG
    '''
    if request.method == "POST":
        row=None

        if 'adminID' in request.POST.keys():
            adminID=request.POST['adminID']

            try:
                row=Survey.objects.get(adminID=request.POST['adminID'])
            except:
                return HttpResponse(
                    json.dumps({
                        "success": False,
                        "errorMSG": "Próba odwołania do nieistniejącego rekordu"
                    }),
                    content_type="application/json"
                )

        else:
            if(request.session['member_id'] is None):
                return HttpResponse(
                    json.dumps({
                        "success": False,
                        "errorMSG": "Próba wykonania zapisu bez logowania"
                    }),
                    content_type="application/json"
                )

            row=Survey()

            adminID_t=None

            while True:
                adminID_t=idGenerator()
                try:
                    # make sure that adminID is unique
                    Survey.objects.get(adminID=adminID_t)
                except:
                    break

            row.adminID=adminID_t

            userID=None

            while True:
                userID=idGenerator()
                try:
                    # make sure that adminID is unique
                    Survey.objects.get(userID=userID)
                except:
                    break

            row.userID=userID

        try:
            row.user=SurveyUser.objects.get(id=request.session['member_id'])
        except:
            pass

        try:

            row.title=request.POST['title']
            row.description=request.POST['description']

            dates=[]


            for key in request.POST.keys():
                try:
                    if "dateTimeFrom" in key:
                        secondKey=key.replace("From","To")
                        fromDate=parser.parse(request.POST[key])
                        toDate=parser.parse(request.POST[secondKey])

                        if fromDate > toDate:
                            return HttpResponse(
                                json.dumps({
                                    "success": False,
                                    "errorMSG": "Błędne przediały czasowe"
                                }),
                                content_type="application/json"
                            )
                        dates.append((fromDate,toDate))
                except:
                    pass

            row.fields=dates
            row.save()

            return HttpResponse(
                json.dumps({
                    "adminID" : row.adminID,
                    "success": True
                }),
                content_type="application/json"
            )

        except:
            return HttpResponse(
                json.dumps({
                    "success": False,
                    "errorMSG": "Błąd odczytu i zapisu do bazy danych"
                }),
                content_type="application/json"
            )
    else:
        return HttpResponse(
            json.dumps({
                "success": False,
                "errorMSG": "Błąd metody przekazywania danych"
            }),
            content_type="application/json"
        )


