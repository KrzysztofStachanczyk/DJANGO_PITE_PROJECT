from django.shortcuts import render
from datetime import datetime, timedelta
from ..models import *
import string
import random
from dateutil import parser
from django.core.urlresolvers import reverse
from django.utils import timezone
import json
from django.http import HttpResponse

def plotDataRequestAjax(request):
    if request.POST:
        typ=request.POST['typ']
        f_date=request.POST['from_date']
        t_date=request.POST['to_date']
        from_date= datetime.strptime(f_date,'%m/%d/%Y %I:%M %p')
        to_date= datetime.strptime(t_date,'%m/%d/%Y %I:%M %p')
        delta=to_date - from_date
        delta=delta.days
        daynum = []
        if typ == 'users_plot':
            daynum.append(['Dzien', 'ilosc zarejestrowanych uzytkownikow w danym dniu'])
            for i in range(1, delta+1):
                recently = to_date - timedelta(days=i)
                django_data = SurveyUser.objects.all().exclude(
                sign_in_date__gte=recently+timedelta(days=1)).filter(
                sign_in_date__gte=recently)         
                daynum.append([str(i), len(django_data)])
        if typ == 'surveys_plot':
            daynum.append(['Dzien', 'ilosc zalozonych ankiet w danym dniu'])
            for i in range(1, delta+1):
                recently = to_date - timedelta(days=i)
                django_data = Survey.objects.all().exclude(
                creation_date__gte=recently+timedelta(days=1)).filter(
                creation_date__gte=recently)
                daynum.append([str(i), len(django_data)])
        if typ == 'answers_plot':
            daynum.append(['Dzien', 'ilosc udzielonych odpowiedzi w danym dniu'])
            for i in range(1, delta+1):
                recently = to_date - timedelta(days=i)
                django_data = Answer.objects.all().exclude(
                creation_date__gte=recently+timedelta(days=1)).filter(
                creation_date__gte=recently)
                daynum.append([str(i), len(django_data)])
        if typ == 'views_plot':
            daynum.append(['Dzien', 'ilosc odwiedzin w danym dniu'])
            for i in range(1, delta+1):
                recently = to_date - timedelta(days=i)
                django_data = UserView.objects.all().exclude(
                view_date__gte=recently+timedelta(days=1)).filter(
                view_date__gte=recently)
                daynum.append([str(i), len(django_data)])
        if typ == 'user_menu_plot':
            if 'member_id' in request.session:
                row = SurveyUser.objects.get(id=request.session['member_id'])
                mySurveys = Survey.objects.filter(user=row)
                daynum.append(['Dzien', 'ilosc utworzonych przez ciebie ankiet w danym dniu'])
                for i in range(1, delta+1):
                    recently = to_date - timedelta(days=i)
                    django_data = mySurveys.exclude(
                    creation_date__gte=recently+timedelta(days=1)).filter(
                    creation_date__gte=recently)
                    daynum.append([str(i), len(django_data)])
        return HttpResponse(
            json.dumps({'data':daynum}),
            content_type="application/json"
            )
        