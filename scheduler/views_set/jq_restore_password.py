from django.shortcuts import render
from ..models import *
import random
from django.contrib.auth.hashers import make_password

from django.http import HttpResponse

import json
import re

def debug_sent_mail(title,message,connectedEmail,target,fail_silently):
    '''
    Funkcja do nadpisywania send_mail przy debugowaniu
    :param title:
    :param message:
    :param connectedEmail:
    :param target:
    :param fail_silently:
    :return:
    '''
    print("Sending password recovery mail to: " + str(target) + "\n\ttitle:" + str(title) + "\n\tmessage:" + str(message))


def jqRestorePassword(request):
    '''
    Renderuj panel do odzyskiwania hasła
    :param request:
    :return:
    '''
    return render(request, 'jq_restore_password.html')

def jqSentRestoreEmailRequest(request):
    '''
    Wysyła email z kodem do odzyskania hasla na podany email
    (dla celów dev kod == 12345678)
    :param request: zapytanie POST z polami:
        - email
    :return:
        JSON z informacją o statusie żądania zawiera pola:
        - success - czy wysłanie maila się powiodło
        - errorMSG - ogólne informacje o błędzie
        - emailMessage - opis błędów adresu email
    '''

    # TODO usun jesli skonfigurowano klienta (w settings.py)
    send_mail = debug_sent_mail

    if request.method == 'POST':
        # sprawdzanie czy user z podanym adresem email istnieje
        try:
            user = SurveyUser.objects.get(email=request.POST['email'])
        except:

            #informowanie o błędnym adresie email
            return HttpResponse(
                json.dumps({
                    "emailMessage": "Podany email nie został odnaleziony.",
                    "errorMSG": "Błąd danych wejściowych.",
                    "success": False
                }),
                content_type="application/json"
            )

        # generowanie kodu do odzyskiwania hasła
        code = ""
        while len(code) != 6:
            i = random.randint(48, 90)
            while i > 57 and i < 65:
                i = random.randint(48, 90)
            code = code + chr(i)

        # TODO usun jesli skonfigurowano klienta (w settings.py)
        code="12345678"

        email = request.POST['email']

        try:
            # wysyłanie mail-a z kodem
            send_mail(
                'PITE zmiana hasła',
                'Prosiłeś o zmianę hasła. Wygenerowany kod to :' + code,
                # TODO zamien na skonfigurowany adres email
                'powiazany email',
                [email],
                fail_silently=False,
            )
        except:
            # przesyłanie informacji o problemach związanych z wysyłaniem maila
            return HttpResponse(
                json.dumps({
                    "errorMSG": "Nie udało się wysłać maila. Spróbuj ponownie.",
                    "success": False
                }),
                content_type="application/json"
            )

        # zapis wygenerewanego kodu i odpowiadającego mu email-a po stronie serwera
        request.session['restore_email'] = email
        request.session['restore_code'] = code

        # informowanie o pomyślnym zakończeniu operacji
        return HttpResponse(
            json.dumps({
                "emailMessage": "Wiadomość została pomyślnie wysłana.",
                "success": True
            }),
            content_type="application/json"
        )

    else:
        return HttpResponse(
                json.dumps({"errorMSG": "Błąd metody przekazywania danych"}),
                content_type="application/json"
            )

def jqSentNewPasswordRequest(request):
    '''
    Kończy proces odzyskiwania hasła
    :param request: żądanie POST z polami:
        - secretCode
        - restorePassword
        - restorePasswordRetype
    :return:
    JSON z informacjami o wyniku operacji zawierający pola:
        - success
        - fields - słownik zawierający komunikaty o błędach dla pól (jeżeli success==False) :
            - secretCode
            - restorePassword
            - restorePasswordRetype
        - errorMSG - ogólny komunika o błędach
    '''
    if request.method == 'POST':
        fields=dict()
        allOk=True

        # sprawdzanie poprawności kodu weryfikującego
        if request.POST['secretCode']!=request.session['restore_code']:
            fields["secretCode"]="Błędny kod weryfikujący"
            allOk=False

        # walidacja poprawności nowych haseł
        if request.POST['restorePassword']!=request.POST['restorePasswordRetype']:
            fields["restorePassword"] = "Hasła nie są zgodne"
            fields["restorePasswordRetype"]=""
            allOk = False

        if not re.match(r"(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}", request.POST["restorePassword"]):
            allOk = False
            fields['restorePassword'] = "Hasło musi zawierać litery o różnej wielkości oraz cyfry i mieć długość co najmniej 6 znaków"
            fields["restorePasswordRetype"] = ""


        if allOk:
            # zmiana hasła
            user=SurveyUser.objects.get(email=request.session['restore_email'])
            user.hash_of_password=make_password(request.POST['restorePassword'],salt=None,hasher='unsalted_md5')
            user.save()

            # usuwanie informacji o kodzie umożliwiającym odzyskanie hasła
            del request.session['restore_email']
            del request.session['restore_code']

            # zwracanie komunikatu o pomyślnym zakończeniu operacji
            return HttpResponse(
                json.dumps({
                    "success": True
                }),
                content_type="application/json"
            )

        else:
            # przekazywanie informacji o powstałych błędach
            return HttpResponse(
                json.dumps({
                    "success": False,
                    "errorMSG": "Błąd wprowadzonych danych",
                    "fields": fields
                }),
                content_type="application/json"
            )
        pass
    else:
        return HttpResponse(
            json.dumps({
                "success": False,
                "errorMSG": "Błąd metody przekazywania danych"}),
            content_type="application/json"
        )