from django.shortcuts import render
from datetime import timedelta
from ..models import *
from django.utils import timezone
from django.http import HttpResponse

import json


def jqGlobalStatistic(request):
    '''
    Generuje stronę zawierającą statystyki witryny
    :param request:
    :return:
    '''

    # pobranie danych z bazy
    userlist = SurveyUser.objects.all()
    numberOfUsers = len(userlist)
    recentusers = userlist.order_by('-sign_in_date')[:1]
    mostRecentUser = recentusers[0].name + ' ' + recentusers[0].surname

    surveylist = Survey.objects.all()
    numberOfSurveys = len(surveylist)

    recentsurvey = surveylist.order_by('-creation_date')[:1]
    mostRecentSurvey = recentsurvey[0].title

    answerlist = Answer.objects.all()
    numberOfAnswers = len(answerlist)

    viewslist = UserView.objects.all()
    numberOfViews = len(viewslist)

    recently = timezone.now() - timedelta(days=1)
    recentSurveys = Survey.objects.all().exclude(creation_date__gte=timezone.now()).filter(creation_date__gte=recently)
    recentAnswers = Answer.objects.all().exclude(creation_date__gte=timezone.now()).filter(creation_date__gte=recently)
    recentUsers = SurveyUser.objects.all().exclude(sign_in_date__gte=timezone.now()).filter(sign_in_date__gte=recently)
    recentViews = UserView.objects.all().exclude(view_date__gte=timezone.now()).filter(view_date__gte=recently)

    rSt = len(recentSurveys)
    rAt = len(recentAnswers)
    rUt = len(recentUsers)
    rVt = len(recentViews)

    return render(request, "jq_global_statistics.html", {
        'numberOfUsers': numberOfUsers,
        'mostRecentUser': mostRecentUser,
        'numberOfSurveys': numberOfSurveys,
        'mostRecentSurvey': mostRecentSurvey,
        'numberOfAnswers': numberOfAnswers,
        'numberOfViews': numberOfViews,
        'numberOfUsersToday': rUt,
        'numberOfSurveysToday': rSt,
        'numberOfAnswersToday': rAt,
        'numberOfViewsToday': rVt
    })

def addViewNumberRequestAjax(request):
    try:
        tmp=request.session['unique']
    except:
        request.session['unique'] = True
        UserView().save()
    return HttpResponse(
            json.dumps({}),
            content_type="application/json"
            )