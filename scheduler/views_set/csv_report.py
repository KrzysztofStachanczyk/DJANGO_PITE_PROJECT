from django.http import HttpResponse
from ..models import *
import csv


def csvReport(request):
    '''
    Generuje raport CSV o udzielonych odpowiedziach
    :param request: Przekazane przy pomocy metody GET dane
        - adminID - identyfikator administratora ankiety
    :return:
        - plik CSV z danymi (lub komunikatem o błędzie)
    '''
    row=None

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = "attachment; filename=report.csv"
    writer = csv.writer(response)

    if 'adminID' in request.GET.keys():
        adminID=request.GET['adminID']
        try:
            row=Survey.objects.get(adminID=request.GET['adminID'])
        except:
            writer.writerow(["Podany przez Ciebie numer ID nie występuje w bazie ankiet"])
        else:
            listOfAnswers=[]
            answers=Answer.objects.filter(survey=row)

            for fromDate,toDate in row.fields:
                listOfMatches=[]
                for answer in answers:
                    try:
                        if answer.answer[(fromDate,toDate)]:
                            listOfMatches.append((answer.name,answer.surname,answer.email))
                    except:
                        pass
                listOfAnswers.append((fromDate,toDate,listOfMatches))

            writer.writerow([row.title])
            writer.writerow(["Od","Do","Imię","Nazwisko","Email"])
            for fromDate, toDate, fitUsers in listOfAnswers:
                writer.writerow([fromDate, toDate])
                for name, surname, email in fitUsers:
                    writer.writerow([" ", " ", name, surname, email])
    else:
        writer.writerow(["Nie przekazano identyfikatora ankiety (adminID)"])
    return response