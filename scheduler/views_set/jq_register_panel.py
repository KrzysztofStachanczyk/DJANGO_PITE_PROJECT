from django.contrib.auth.hashers import make_password
from django.db import IntegrityError

import re
from ..models import SurveyUser

from django.http import HttpResponse
import json

def registerAjaxRequest(request):
    '''
    Przeprowadza proces rejestracji nowego użytkownika
    :param request: żądanie POST z wypełnionymi polami:
        - name
        - surname
        - email
        - password
        - rePassword
    :return:
    JSON - zawierający informacje o statusie operacji z polami:
        - successfullRegistration
        - errorMSG - ogólny opis błędu
        - fieldState - słownik zawierający komunikaty o błędach dla poszczególnych pól
    '''
    if request.method == 'POST':
        fieldState = dict()
        try:
            # walidacja poprawności podanych danych na podstawie RegEXP
            allCorrect=True

            if not re.match(r"^[A-Za-z àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆŠŽ]{3,20}$",request.POST['name']):
                allCorrect=False
                fieldState["inputName"]="Imię musi składać się z 3-20 liter"

            if not re.match(r"^[A-Za-z àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆŠŽ∂ð,.'-]{3,50}$",request.POST['surname']):
                allCorrect=False
                fieldState['inputSurname']="Nazwisko musi składać się z 3-50 liter i znaków"

            if not re.match(r"^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$", request.POST['email']):
                allCorrect=False
                fieldState['inputEmail']="Wpisz e-mail w poprawnym formacie"

            if not re.match(r"(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}",request.POST["password"]):
                allCorrect=False
                fieldState['inputPassword']="Hasło musi zawierać litery o różnej wielkości oraz cyfry i mieć długość co najmniej 6 znaków"

                if request.POST["password"]!=request.POST["rePassword"]:
                    allCorrect=False
                    fieldState['inputPassword']="Podane hasła nie są zgodne"
                    fieldState['inputPasswordRetype'] = ""


            if allCorrect:
                # tworzenie rekordu reprezentującego user-a
                row=SurveyUser(name=request.POST['name'],surname=request.POST['surname'],email=request.POST['email'])
                row.hash_of_password=make_password(request.POST['password'],salt=None,hasher='unsalted_md5')
                row.save()

                # automatyczne logowanie
                user = SurveyUser.objects.get(email=request.POST['email'])
                request.session['member_id'] = user.id
                request.session['member_name'] = user.name
                request.session['member_surname'] = user.surname

                # zwracanie informacji o pomyślnym zakończeniu operacji
                return HttpResponse(
                    json.dumps({"fieldState": fieldState,
                                "successfullRegistration": True
                                }),
                    content_type="application/json"
                )

            else:
                # informowanie o błędach walidacji
                return HttpResponse(
                    json.dumps({"fieldState": fieldState,
                                "successfullRegistration": False,
                                "errorMSG": "Wprowadzone dane są błędne"
                                }),
                    content_type="application/json"
                )


        except IntegrityError:
            # wysyłanie komunikatu o tym, że podany email istnieje już w bazie danych
            fieldState['inputEmail'] = "Podany email istnieje"

            return HttpResponse(
                json.dumps({"fieldState": fieldState,
                            "successfullRegistration": False,
                            "errorMSG": "Podany email już istnieje"
                            }),
                content_type="application/json"
            )

        except:
            return HttpResponse(
                json.dumps({
                            "successfullRegistration": False,
                            "errorMSG": "Nieobsługiwany błąd"
                            }),
                content_type="application/json"
            )

    else:
        return HttpResponse(
            json.dumps({"errorMSG": "Błąd metody przekazywania danych"}),
            content_type="application/json"
        )