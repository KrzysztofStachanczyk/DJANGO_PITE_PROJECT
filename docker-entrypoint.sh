#!/bin/bash
echo "------------------------------------"
echo "           Base migrations"
echo "------------------------------------"
python3 manage.py migrate                 


echo "------------------------------------"
echo "        Collect static files"
echo "------------------------------------"
python3 manage.py collectstatic --noinput  


echo "------------------------------------"
echo "           Server run"
echo "------------------------------------"
python3 manage.py runserver 0.0.0.0:8000
