        $(document).ready(
            function(){
                $("#termsAndConditions").load("hidden/jqTermsAndConditions #pageContent");
            }
        );

        $('#jq_register_form').on('submit', function(event){
            event.preventDefault();

            if(isAccepted()){
                $.ajax({
                    url : "hidden/jqRegisterPanel/registerRequest",
                    type : "POST",
                    data : $("#jq_register_form").serialize(),

                    success : function(json) {
                        markFieldAsCorrect("inputName");
                        markFieldAsCorrect("inputSurname");
                        markFieldAsCorrect("inputEmail");
                        markFieldAsCorrect("inputPassword");
                        markFieldAsCorrect("inputPasswordRetype");

                        if(json["successfullRegistration"]==true){
                            loadLoginPane();
                            loadDefaultBody();
                            toastr.success("Rejestracja przebiegła pomyślnie");
                        }
                        else{
                            if(json["fieldState"]!=null){
                                Object.keys(json["fieldState"]).forEach(function(key){
                                    markFieldAsIncorrect(key,json["fieldState"][key]);
                                });
                            }
                            else{
                                 toastr.error(json['errorMSG']);
                            }
                        }
                    }
                });

            }
        });

        function isAccepted(){
            var checked=$("#termsAndConditionsAccepted").is(":checked");
            if(checked){
                $("#termsAndConditionsAccepted").closest( ".form-group" ).removeClass("has-error");
                $("#termsAndConditionsAccepted").closest( ".form-group" ).removeClass("has-feedback");
                $("#termsAndConditionsAccepted").parent().children( ".help-block" ).html("");
            }
            else{
                $("#termsAndConditionsAccepted").closest( ".form-group" ).addClass("has-error");
                $("#termsAndConditionsAccepted").closest( ".form-group" ).addClass("has-feedback");
                $("#termsAndConditionsAccepted").parent().children( ".help-block" ).html("Przeczytaj i zaakceptuj regulamin");
            }
            return checked;
        }