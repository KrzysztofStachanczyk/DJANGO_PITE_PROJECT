
        $('#jq_contactForm').on('submit', function(event){
            event.preventDefault();
                var allValid=true;

                var name=$('#contactName').val();
                var surname=$('#contactSurname').val();
                var email=$('#contactEmail').val();


                if(isName(name)){
                    markFieldAsUnknownState("contactName");
                }
                else{
                    allValid=false;
                    markFieldAsIncorrect("contactName","Imię musi składać się z 3-20 liter");
                }

                if(isSurname(surname)){
                    markFieldAsUnknownState("contactSurname");
                }
                else{
                    allValid=false;
                    markFieldAsIncorrect("contactSurname","Nazwisko musi składać się z 3-50 liter i znaków");
                }

                if(isEmail(email)){
                    markFieldAsUnknownState("contactEmail");
                }
                else{
                    allValid=false;
                    markFieldAsIncorrect("contactEmail","Wpisz e-mail w poprawnym formacie");
                }

                if(!allValid){
                    return;
                }
                markFieldAsUnknownState("contactTopic");


                $.ajax({
                    url : "hidden/jqContactForm/contactSentMailRequest",
                    type : "POST",
                    data : {
                        name : name,
                        surname : surname,
                        email : email,
                        text : $('#contactText').val(),
                        topic : $('#contactTopic').val(),
                        captcha_0 : $('#id_captcha_0').val(),
                        captcha_1 : $('#id_captcha_1').val(),
                        csrfmiddlewaretoken: getCookie('csrftoken')
                    },

                    success : function(json) {
                        if(json["successful"]==true){
                            toastr.success("Dziękujemy za skontatkowanie się z nami. \nOdpowiedź otrzymasz najszybciej jak się da.");
                        }
                        else{
                            toastr.error(json["errorMSG"]);
                        }

                        $("#captchaFieldElement").load("hidden/jqContactForm #captchaFieldElement > ");

                        markFieldAsCorrect("contactName");
                        markFieldAsCorrect("contactSurname");
                        markFieldAsCorrect("contactEmail");
                        markFieldAsCorrect("contactTopic");

                        if(json["fieldState"]!=null){
                            var fieldMessages=Object.keys(json["fieldState"]);

                            fieldMessages.forEach(function(key){
                                markFieldAsIncorrect(key,json["fieldState"][key]);
                            });
                        }
                    }
                });
        });
