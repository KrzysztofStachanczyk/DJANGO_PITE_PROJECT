 $('#statsLink').bind('click', function(){
       setTimeout(function(){ drawAll(); }, 500);
  });

$('#jq_accoutDataChangeForm').on('submit', function(event){
    event.preventDefault();

    $.ajax({
        url: "hidden/jqAccountManagement/sentChangeUserdataRequest",
        type : "POST",
        data : $("#jq_accoutDataChangeForm").serialize(),

        success: function(json){
            markFieldAsUnknownState("changeSurnameInput");
            markFieldAsUnknownState("changeNameInput");

            if(json['success']==true){
                $("#loginPane").load("hidden/jqLoginPane");
                $("#accountSumUp").load("hidden/jqAccountManagement #accountSumUp >");
                toastr.success("Zmiana danych przebiegła pomyślnie");
            }
            else{
                if(json["fields"]!=null){
                    markFieldAsCorrect("changeSurnameInput");
                    markFieldAsCorrect("changeNameInput");

                    var fieldMessages=Object.keys(json["fields"]);

                    fieldMessages.forEach(function(key){
                                markFieldAsIncorrect(key,json["fields"][key]);
                            });
                }
                else{
                    toastr.error(json['errorMSG']);
                }
            }
        }
    });
});

$('#jq_accoutPasswordChangeForm').on('submit', function(event){
    event.preventDefault();

    $.ajax({
        url: "hidden/jqAccountManagement/sentChangeUserPasswordRequest",
        type : "POST",
        data : $("#jq_accoutPasswordChangeForm").serialize(),

        success: function(json){
            markFieldAsUnknownState("changeOldPasswordInput");
            markFieldAsUnknownState("changeNewPasswordInput");
            markFieldAsUnknownState("changeNewPasswordRetypeInput");

            if(json['success']==true){
                toastr.success("Zmiana hasła zakończona sukcesem");
            }
            else{
                if(json["fields"]!=null){
                    var fieldMessages=Object.keys(json["fields"]);

                    fieldMessages.forEach(function(key){
                                markFieldAsIncorrect(key,json["fields"][key]);
                            });
                }
                else{
                    toastr.error(json['errorMSG']);
                }
            }
        }
    });
});

$('#accountSurveyList').on('click', '.removeSurvey',function(event){
    event.preventDefault();
    console.log( $(this).attr('href'));
    $.ajax({
        url: "hidden/jqAccountManagement/sentRemoveSurveyRequest",
        type : "POST",
        data : {
            adminID: $(this).attr('href'),
            csrfmiddlewaretoken: getCookie('csrftoken')
        },

        success: function(json){
                if(json["success"]==true){
                    toastr.success("Usuwanie ankiety zakończono pomyślnie");
                    $("#accountSurveyList").load("hidden/jqAccountManagement #accountSurveyList >");
                }
                else{
                    toastr.error(json['errorMSG']);
                }
        }
    });
});