function reloadcaptha(){
        var form = document.getElementsByClassName("form_with_captcha")[0];
        var url = location.protocol + "//" + window.location.hostname + ":"
                  + location.port + "/captcha/refresh/";
        // Make the AJAX-call
        $.getJSON(url, {}, function(json) {
            $(form).find('input[name="captcha_0"]').val(json.key);
            $(form).find('input[name="captcha_1"]').val('');
            $(form).find('img.captcha').attr('src', json.image_url);
        });

        return false;
    }