$('#jq_login_form').on('submit', function(event){
            event.preventDefault();
            $.ajax({
                    url : "hidden/jqLoginElements/loginRequest",
                    type : "POST",
                    data : $('#jq_login_form').serialize(),

                    success : function(json) {
                        if(json["successfullLogin"]==true){
                            loadLoginPane();
                            loadDefaultBody();
                        }

                        else{
                            if(json["fieldState"]!=null){
                                var fieldMessages=Object.keys(json["fieldState"]);

                                markFieldAsCorrect("inputLoginEmail");
                                markFieldAsIncorrect("inputLoginPassword","");

                                fieldMessages.forEach(function(key){
                                    markFieldAsIncorrect(key,json["fieldState"][key]);
                                });
                            }
                            else{
                                toastr.error(json['errorMSG']);
                            }
                        }
                    }
            });
        });
