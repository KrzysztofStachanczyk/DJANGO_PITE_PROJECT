function return_chart_id(plot_name){
    var dict = {};
    dict['users_plot'] = 'chart3';
    dict['surveys_plot']='chart1';
    dict['answers_plot']='chart2';
    dict['views_plot']='chart4';
    dict['user_menu_plot']='menu_chart';
    return dict[plot_name];
}
function return_plot_name_part(plot_name){
    var dict = {};
    dict['users_plot'] = 'rejestracji użytkowników';
    dict['surveys_plot']='założonych ankiet';
    dict['answers_plot']='udzielonych odpowiedzi';
    dict['views_plot']='odwiedzin na stronie';
    dict['user_menu_plot']=' utworzonych przez ciebie ';
    return dict[plot_name];
}
function drawChart(chart_id,data,from_date,to_date,type) {

        var data_from_django = data;
        data=google.visualization.arrayToDataTable(data);
        var options = {
          title: 'Ilość '+type+' pomiedzy '+to_date+ ' a '+from_date,
          vAxis: {title: 'Ilość'},
          legend: { position: 'bottom' },
          isStacked: true
        };

        var chart = new google.visualization.SteppedAreaChart(document.getElementById(chart_id));
        chart.draw(data, options);

        
}
 $('#ps_u, #ps_s, #ps_a, #ps_v').on('submit', function(event){
            event.preventDefault();
            var $this=$(this);
            var typ=$this.find('[name=plot_type]').val();
            var from=$this.find('[name=dateTimeFrom]').val();
            var to=$this.find('[name=dateTimeTo]').val();
            $.ajax({
                    url : "hidden/jqGetPlotData",
                    type : "POST",
                    data : {
                        typ: typ,
                        from_date:from,
                        to_date: to,
                        csrfmiddlewaretoken: getCookie('csrftoken')
                    },
                success : function(json) {
                        if(json['data'].length < 2){return;}
                        var chart_id=return_chart_id(typ);
                        drawChart(chart_id,json['data'],from,to,return_plot_name_part(typ));
                    }
            });
      });
      
  $(function () {
        var now= new Date();
        $('#datetimepickertu').datetimepicker({
            maxDate: now,
            defaultDate: now
        });
        $('#datetimepickerfu').datetimepicker({
            maxDate: new Date().setDate(now.getDate()-1),
            defaultDate: new Date().setDate(now.getDate()-10),
            useCurrent: false
        });
        $("#datetimepickertu").on("dp.change", function (e) {
            $('#datetimepickerfu').data("DateTimePicker").maxDate(e.date);
        });
        
        
        $('#datetimepickerts').datetimepicker({
            maxDate: now,
            defaultDate: now
        });
        
        $('#datetimepickerfs').datetimepicker({
            maxDate: new Date().setDate(now.getDate()-1),
            defaultDate: new Date().setDate(now.getDate()-10),
            useCurrent: false
        });
        $("#datetimepickerts").on("dp.change", function (e) {
            $('#datetimepickerfs').data("DateTimePicker").maxDate(e.date);
        });
        
        
        $('#datetimepickerta').datetimepicker({
            maxDate: now,
            defaultDate: now
        });
        $('#datetimepickerfa').datetimepicker({
            maxDate: new Date().setDate(now.getDate()-1),
            defaultDate: new Date().setDate(now.getDate()-10),
            useCurrent: false
        });
        $("#datetimepickerta").on("dp.change", function (e) {
            $('#datetimepickerfa').data("DateTimePicker").maxDate(e.date);
        });
        
        $('#datetimepickertv').datetimepicker({
            maxDate: now,
            defaultDate: now
        });
        $('#datetimepickerfv').datetimepicker({
            maxDate: new Date().setDate(now.getDate()-1),
            defaultDate: new Date().setDate(now.getDate()-10),
            useCurrent: false
        });
        $("#datetimepickertv").on("dp.change", function (e) {
            $('#datetimepickerfv').data("DateTimePicker").maxDate(e.date);
        });
        
    });
    $(document).ready(function(){
            $(".chartInPage").height($("#pageContent").width()*3/6);
            $('#btn_users').click();
            $('#btn_surveys').click();
            $('#btn_answers').click();
            $('#btn_views').click();
    });

    $( window ).resize(
            function(){
                $(".chartInPage").height($("#pageContent").width()*3/6);
                $('#btn_users').click();
                $('#btn_surveys').click();
                $('#btn_answers').click();
                $('#btn_views').click();

            });