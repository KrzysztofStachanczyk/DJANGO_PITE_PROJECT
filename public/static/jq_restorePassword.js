$('#jq_restorePassword_email').on('submit', function(event){
    event.preventDefault();


    if(!isEmail($('#restoreEmail').val())){
        markFieldAsIncorrect("restoreEmail","Wpisz e-mail w poprawnym formacie");
        $("#secretCode").prop('disabled', true);
        $("#restorePassword").prop('disabled', true);
        $("#restorePasswordRetype").prop('disabled', true);
        $("#step2Submit").prop('disabled', true);

        return;
    }

    $.ajax({
        url : "hidden/jqRestorePassword/sentRestoreEmailRequest",
        type : "POST",
        data : $("#jq_restorePassword_email").serialize(),

        success : function(json) {

            if(json['success']==false){
                markFieldAsIncorrect("restoreEmail",json['emailMessage']);
                $("#secretCode").prop('disabled', true);
                $("#restorePassword").prop('disabled', true);
                $("#restorePasswordRetype").prop('disabled', true);
                $("#step2Submit").prop('disabled', true);
            }
            else{
                markFieldAsCorrectWithText("restoreEmail",json['emailMessage']);
                $("#secretCode").prop('disabled', false);
                $("#restorePassword").prop('disabled', false);
                $("#restorePasswordRetype").prop('disabled', false);
                $("#step2Submit").prop('disabled', false);
                $("#step1Submit").prop('disabled', true);
                $("#restoreEmail").prop('disabled', true);
            }
        }
    });
});



$('#jq_restorePassword_newPassword').on('submit', function(event){
    event.preventDefault();

    $.ajax({
        url : "hidden/jqRestorePassword/sentNewPasswordRequest",
        type : "POST",
        data : $("#jq_restorePassword_newPassword").serialize(),

        success : function(json) {

            if(json['success']==true){
                toastr.success("Zmiana hasła zakończona sukcesem");
                markFieldAsCorrect("secretCode");
                markFieldAsCorrect("restorePassword");
                markFieldAsCorrect("restorePasswordRetype");

                $("#secretCode").prop('disabled', true);
                $("#restorePassword").prop('disabled', true);
                $("#restorePasswordRetype").prop('disabled', true);
                $("#step2Submit").prop('disabled', true);
            }
            else{
                if(json["fields"]!=null){
                    markFieldAsCorrect("secretCode");
                    markFieldAsCorrect("restorePassword");
                    markFieldAsCorrect("restorePasswordRetype");

                    var fieldMessages=Object.keys(json["fields"]);

                    fieldMessages.forEach(function(key){
                        markFieldAsIncorrect(key,json["fields"][key]);
                        });
                }
                else{
                   toastr.error(json['errorMSG']);
                }
            }
        }
    });
});





